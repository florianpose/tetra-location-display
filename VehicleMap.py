# ----------------------------------------------------------------------------
#
# Vehicles Data
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import numpy as np

import tempfile
import shutil
import os
import json
import datetime

from Vehicle import Vehicle
from MobileTermination import MobileTermination


# ----------------------------------------------------------------------------

class VehicleMap:

    # ------------------------------------------------------------------------

    def __init__(self, config, logger, mapWidget=None):
        self.config = config
        self.logger = logger
        self.tsiMap = {}
        self.mapWidget = mapWidget
        self.statePath = config.get("display", "state_path",
                                    fallback="state.json")

    def loadState(self):
        if not os.path.exists(self.statePath):
            return

        with open(self.statePath) as jsonFile:
            for state in json.load(jsonFile):
                mt = MobileTermination(state['tsi'], state['name'])
                vehicle = Vehicle(self.config, self.logger, mt)
                vehicle.loadState(state)
                self.tsiMap[mt.identifier] = vehicle
                if self.mapWidget:
                    vehicle.moving.connect(self.mapWidget.update)

    def saveState(self):
        stateList = []
        for v in self.tsiMap.values():
            stateList.append(v.dumpState())
        tmp = tempfile.NamedTemporaryFile(mode='w', delete=False)
        tmp.write(json.dumps(stateList, indent=4))
        tmp.close()
        shutil.move(tmp.name, self.statePath)

    def setPosition(self, mt, latDeg, lonDeg):
        vehicle = self.getVehicle(mt)
        now = datetime.datetime.now()
        vehicle.setPosition(latDeg, lonDeg, now)
        for v in self.getPositionVehicles(now):
            self.logger.debug(v)

    def setStatus(self, mt, status):
        vehicle = self.getVehicle(mt)
        now = datetime.datetime.now()
        vehicle.setStatus(status, now)
        for v in self.getPositionVehicles(now):
            self.logger.debug(v)

    def getVehicle(self, mt):
        if mt.identifier in self.tsiMap:
            vehicle = self.tsiMap[mt.identifier]
        else:
            vehicle = Vehicle(self.config, self.logger, mt)
            self.tsiMap[mt.identifier] = vehicle
            if self.mapWidget:
                vehicle.moving.connect(self.mapWidget.update)
        return vehicle

    def getPositionVehicles(self, now):
        vehicles = self.tsiMap.values()
        return list(filter(lambda v: v.positionValid(now), vehicles))

    def getPositionRange(self, now):
        vehicles = self.getPositionVehicles(now)

        if not vehicles:
            return (None, None, None, None)

        minLatDeg = np.min([v.latDeg for v in vehicles])
        maxLatDeg = np.max([v.latDeg for v in vehicles])
        minLonDeg = np.min([v.lonDeg for v in vehicles])
        maxLonDeg = np.max([v.lonDeg for v in vehicles])

        return (minLatDeg, maxLatDeg, minLonDeg, maxLonDeg)

# ----------------------------------------------------------------------------
