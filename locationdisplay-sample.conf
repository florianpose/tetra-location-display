;-----------------------------------------------------------------------------
;
; vim: syntax=dosini
;
; Tetra Location Display Sample Configuration
;
; Copyright (C) 2018-2022 Florian Pose
;
; This file is part of Tetra Location Display.
;
; Tetra Location Display is free software: you can redistribute it and/or
; modify it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or (at your
; option) any later version.
;
; Tetra Location Display is distributed in the hope that it will be useful, but
; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
; FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
; details.
;
; You should have received a copy of the GNU General Public License along with
; Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
;
;-----------------------------------------------------------------------------

[display]

;
; Path for CSV TSSI database
; Default: fugs.csv
;
;csv_path =

;
; File path for persistent vehicle states
; Default: state.json
;
;state_path =

;
; Log path
; Default: /var/log/locationdisplay.log
;
;log_path =

;-----------------------------------------------------------------------------

[multicast]

;
; Multicast group to listen on
; Default: 226.0.0.14
;
;group =

;
; Multicast port to listen on
; Default: 10800
;
;port =

;
; Name of the interface to listen on
; Default: (first non-loopback)
;
;interface =

;-----------------------------------------------------------------------------

[map]

;
; Directory with map tiles
; Default: tiles (relative to current directory)
;
;tiles_dir =

;
; Default view coordinates
; Used on startup before a valid position is received.
; Default: 0.0
;
;default_lat =
;default_lon =

;
; Default zoom level
; Used on startup before a valid position is received.
; see http://wiki.openstreetmap.org/wiki/Zoom_levels
; Default: 15
;
;default_zoom =

;
; Maximum zoom level in auto-pan mode.
; see http://wiki.openstreetmap.org/wiki/Zoom_levels
; Default: 17
;
;max_zoom =

;-----------------------------------------------------------------------------

[area]

;
; Dangerous area center
; Default: 0.0
;
;center_lat =
;center_lon =

;
; Radius of dangerous area
; Multiple radii from inner to outer
;
;radius0 =
;radius1 =
;...

;
; Color of dangerous area
; Format ARGB: #40aaaa00
;
;color0 =
;color1 =
;...

;-----------------------------------------------------------------------------
