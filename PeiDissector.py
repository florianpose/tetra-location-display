# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Tetra PEI Dissector
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import re
import math
import bitstring
from MobileTermination import MobileTermination

# ----------------------------------------------------------------------------


class StateInfo:

    def __init__(self, ais, caller, length):
        self.ais = ais
        self.mt = MobileTermination(caller)
        self.bitLength = length

# ----------------------------------------------------------------------------


class Position:

    reasons = {
        0: "Power ON",
        1: "Powered OFF",
        2: "Emergency",
        3: "Push-to-talk",
        4: "Status",
        5: "Transmit inhibit mode ON",
        6: "Transmit inhibit mode OFF",
        7: "TMO ON",
        8: "DMO ON",
        9: "Enter service",
        10: "Service loss",
        11: "Cell reselection",
        12: "Low battery",
        13: "Connected to car kit",
        14: "Disconnected from car kit",
        15: "Asks for transfer initialization configuration",
        16: "Arrival at destination",
        17: "Arrival at defined location",
        18: "Approaching defined location",
        19: "SDS type-1 entered",
        20: "User application initiated",
        32: "Response to location request",
        129: "Interval",
        130: "Distance",
        }

    def __init__(self, mt, lat, lon, reason):
        self.mt = mt
        self.lat = lat
        self.lon = lon
        self.reason = reason

    def __repr__(self):
        reason = self.reasons.get(self.reason, f'[{self.reason}]')
        url = ('https://www.openstreetmap.org/?'
               f'mlat={self.lat:.5f}&mlon={self.lon:.5f}#'
               f'map=17/{self.lat:.5f}/{self.lon:.5f}')
        return (f'Position {self.mt} {self.lat:.5f} {self.lon:.5f} {reason}\n'
                f'{url}')

# ----------------------------------------------------------------------------


class Status:

    statusTexts = {
        0: "Priorisierter Sprechwunsch",
        1: "Einsatzbereit über Funk",
        2: "Einsatzbereit Wache",
        3: "Einsatz übernommen",
        4: "Einsatzort an",
        5: "Sprechwunsch",
        6: "Nicht einsatzbereit",
        }

    def __init__(self, mt, status):
        self.mt = mt
        self.status = status

    def __repr__(self):
        text = self.statusTexts.get(self.status, '')
        return f'Status {self.mt} - {self.status} {text}'

# ----------------------------------------------------------------------------


class State:

    def run(self):
        assert 0, "run not implemented"

    def next(self, line):
        assert 0, "next not implemented"

# ----------------------------------------------------------------------------


class Waiting(State):

    # +CTSDSR: 108,262100106438029,1,262100102440320,1,88,1
    # +CTSDSR: <AI service>, [<calling party identity>],
    #          [<calling party identity type>], <called party identity>,
    #          <called party identity type>, <length>,
    #          [<end to end encryption>]<CR><LF>user data
    # Attention: If last field is missing the comma will be missing as well
    stateRe = re.compile(('\\+CTSDSR: '
                          '(?P<ais>[0-9]*),'     # AI service
                          '(?P<caller>[0-9]*),'  # calling party identity
                          '([0-9]*),'            # calling party identity type
                          '(?P<called>[0-9]*),'  # called party identity
                          '([0-9]*),'            # called party identity type
                          '(?P<length>[0-9]*)'   # length (in bit)
                          '(,([0-9]*))?'         # end to end encryption
                          ))

    def __init__(self, logger):
        self.logger = logger

    def run(self):
        pass

    def next(self, line):
        ma = self.stateRe.fullmatch(line)
        if ma:
            si = StateInfo(int(ma.group('ais')), ma.group('caller'),
                           int(ma.group('length')))
            return UserData(si, self.logger), None
        else:
            return Waiting(self.logger), None

# ----------------------------------------------------------------------------


class UserData(State):

    def __init__(self, stateInfo, logger):
        self.stateInfo = stateInfo
        self.logger = logger

    def run(self):
        pass

    def next(self, line):
        expectedHexLength = math.ceil(self.stateInfo.bitLength / 4)
        assert len(line) == expectedHexLength, \
            (f'unexpected length {len(line)} / {expectedHexLength}'
             f' (bit length {self.stateInfo.bitLength})')

        if len(line) % 2:
            line += '0'  # fromhex complains about odd number of characters
        bs = bitstring.ConstBitStream(bytes.fromhex(line))
        protocolIdentifier = bs.read('uint:8')

        if protocolIdentifier == 10:
            # Detected Location Information Protocol (LIP)

            pduType = bs.read('int:2')
            if pduType == 0:  # short location report
                bs.read('uint:2')  # timeElapsed
                longitude = bs.read('uint:25') * 360.0 / 2**25
                latitude = bs.read('int:24') * 180.0 / 2**24
                bs.read('uint:3')  # posError
                bs.read('uint:7')  # horVelocity
                bs.read('uint:4')  # direction
                additionalData = bs.read('uint:1')
                if additionalData == 0:
                    reasonForSending = bs.read('uint:8')
                else:
                    reasonForSending = 0

                result = Position(self.stateInfo.mt, latitude, longitude,
                                  reasonForSending)
                return Waiting(self.logger), result

            elif pduType == 1:  # long location report
                bs.read('uint:4')  # pduTypeExt

                # time data
                timeType = bs.read('uint:2')
                if timeType == 0:
                    pass
                elif timeType == 1:
                    bs.read('uint:2')  # timeElapsed
                elif timeType == 2:
                    bs.read('uint:22')  # timeOfPosition
                else:
                    self.logger.error(
                            f'Time type {timeType} not implemented.')
                    return Waiting(self.logger), None

                # location data
                locationShape = bs.read('uint:4')
                if locationShape == 1:  # point
                    longitude = bs.read('uint:25') * 360.0 / 2**25
                    latitude = bs.read('int:24') * 180.0 / 2**24
                elif locationShape == 5:  # location circ. with alt.
                    longitude = bs.read('uint:25') * 360.0 / 2**25
                    latitude = bs.read('int:24') * 180.0 / 2**24
                    bs.read('uint:6')  # uncertainty
                    bs.read('uint:12')  # altitude
                else:
                    # 2 Location circle
                    # 3 Location ellipse
                    # 4 Location point with altitude
                    # 6 Location ellipse with altitude
                    # 7 Location circle with altitude and altitude uncertainty
                    # 8 Location ellipse with altitude and altitude uncertainty
                    # 9 Location arc
                    # 10 Location point and position error
                    # 11 Reserved
                    # 12 Reserved
                    # 13 Reserved
                    # 14 Reserved
                    # 15 Location shape extension, see note.
                    self.logger.error((f'Location shape {locationShape}'
                                       ' not implemented.'))
                    return Waiting(self.logger), None

                # velocity data
                velocityType = bs.read('uint:3')
                if velocityType == 0:  # no velocity information
                    pass
                elif velocityType == 1:  # horizontal velocity
                    bs.read('uint:7')  # velocity
                elif velocityType == 2:  # horizontal velocity with uncert.
                    bs.read('uint:10')  # velocity
                elif velocityType == 3:  # horizontal and vertical velocity
                    bs.read('uint:15')  # velocity
                elif velocityType == 4:  # hor and vert velocity with uncert.
                    bs.read('uint:21')  # velocity
                elif velocityType == 5:  # hor vel. with direction
                    bs.read('uint:7')  # velocity
                    bs.read('uint:8')  # direction
                elif velocityType == 6:  # hor vel. with dir and uncert.
                    bs.read('uint:21')  # velocity
                elif velocityType == 7:  # hor and ver with dir and uncert.
                    bs.read('uint:32')  # velocity

                bs.read('uint:1')  # ackReq
                additionalData = bs.read('uint:1')
                if additionalData == 0:
                    reasonForSending = bs.read('uint:8')
                else:
                    reasonForSending = 0
                    bs.read('uint:8')  # userData

                result = Position(self.stateInfo.mt, latitude, longitude,
                                  reasonForSending)
                return Waiting(self.logger), result

            else:
                self.logger.error(f'PDU type {pduType} not implemented.')
                return Waiting(self.logger), None

        if protocolIdentifier == 0x80:
            # Detected Status
            status = bs.read('int:8') - 2

            result = Status(self.stateInfo.mt, status)
            return Waiting(self.logger), result

        return Waiting(self.logger), None

# ----------------------------------------------------------------------------


class PeiDissector:

    def __init__(self, logger):
        self.logger = logger
        self.currentState = Waiting(self.logger)
        self.currentState.run()

    def receive(self, line):
        self.logger.debug(repr(line))
        clean_line = line.replace('\x00', '')
        if line != clean_line:
            self.logger.warning('Removed zero bytes')
        try:
            self.currentState, result = self.currentState.next(clean_line)
            self.currentState.run()
        except Exception:
            self.logger.error('Exception on evaluting input line.',
                              exc_info=True)
            self.currentState = Waiting(self.logger)
            self.currentState.run()
            result = None

        return result

# ----------------------------------------------------------------------------
