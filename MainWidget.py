# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Main Widget
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import subprocess

from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QWidget, QVBoxLayout

from MapWidget import MapWidget
from SocketListener import SocketListener
from PeiDissector import PeiDissector, Position, Status
from VehicleMap import VehicleMap
from MobileTermination import MobileTermination


# ----------------------------------------------------------------------------

class MainWidget(QWidget):

    def __init__(self, config, logger, simulation, displayAll):
        super(MainWidget, self).__init__()

        self.config = config
        self.logger = logger
        self.displayAll = displayAll

        MobileTermination.set_path(
            self.config.get("display", "csv_path", fallback="fugs.csv"))

        self.socketListener = SocketListener(self.config, self.logger)
        self.socketListener.readLine.connect(self.readLine)

        self.dissector = PeiDissector(self.logger)
        self.vehicles = VehicleMap(self.config, self.logger)

        # Appearance ---------------------------------------------------------

        self.logger.info('Setting up X server...')

        subprocess.call(['xset', 's', 'off'])
        subprocess.call(['xset', 's', 'noblank'])
        subprocess.call(['xset', 's', '0', '0'])
        subprocess.call(['xset', '-dpms'])

        self.move(0, 0)
        self.resize(1920, 1080)

        self.setWindowTitle('Tetra Location Display')

        self.setStyleSheet("""
            font-size: 12px;
            background-color: rgb(0, 34, 44);
            color: rgb(2, 203, 255);
            font-family: "DejaVu Sans";
            """)

        # Sub-widgets --------------------------------------------------------

        layout = QVBoxLayout(self)
        layout.setSpacing(0)
        layout.setContentsMargins(0, 0, 0, 0)

        self.mapWidget = MapWidget(self, self.config, self.vehicles,
                                   self.logger)
        self.vehicles.mapWidget = self.mapWidget
        layout.addWidget(self.mapWidget)

        self.vehicles.loadState()

        self.logger.info('Setup finished.')

        if simulation:
            mt = MobileTermination('262100108206636')
            result = Position(mt, 51.75, 6.11, 0)
            self.vehicles.setPosition(result.mt, result.lat, result.lon)
            self.vehicles.setStatus(result.mt, 1)
            self.mapWidget.updateMap()

            self.simulationTimer = QTimer(self)
            self.simulationTimer.setInterval(5000)
            self.simulationTimer.setSingleShot(True)
            self.simulationTimer.timeout.connect(self.simulationTimeout)
            self.simulationTimer.start()

    def simulationTimeout(self):
        mt = MobileTermination('262100108206636')
        result = Position(mt, 51.78525, 6.143, 0)
        self.vehicles.setPosition(result.mt, result.lat, result.lon)
        self.vehicles.setStatus(result.mt, 2)
        self.mapWidget.updateMap()

    def saveState(self):
        self.vehicles.saveState()

    def readLine(self, line):
        result = self.dissector.receive(line)

        if result is None:
            return

        if result.mt.name is None and not self.displayAll:
            return

        self.logger.info(result)

        if isinstance(result, Position):
            if result.lat and result.lon:
                self.vehicles.setPosition(result.mt, result.lat, result.lon)
                self.mapWidget.updateMap()

        if isinstance(result, Status):
            self.vehicles.setStatus(result.mt, result.status)
            self.mapWidget.updateMap()

# ----------------------------------------------------------------------------
