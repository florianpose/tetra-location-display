# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Socket listener
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

from PyQt5 import QtCore, QtNetwork


# ----------------------------------------------------------------------------

class SocketListener(QtCore.QObject):

    readLine = QtCore.pyqtSignal(str)

    def __init__(self, config, logger):
        super(SocketListener, self).__init__()
        self.config = config
        self.logger = logger

        self.buf = ''

        mcastGroup = self.config.get("multicast", "group",
                                     fallback="226.0.0.14")
        mcastPort = self.config.getint("multicast", "port", fallback=10800)
        mcastIface = self.config.get("multicast", "interface", fallback='')

        if mcastIface:
            iface = QtNetwork.QNetworkInterface.interfaceFromName(mcastIface)
        else:
            allIf = QtNetwork.QNetworkInterface.allInterfaces()
            allIf = list(filter(lambda iface: iface.name() != 'lo', allIf))
            if not len(allIf):
                self.logger.error('No interface found to listen on!')
                return
            if len(allIf) > 1:
                self.logger.warning('Multiple interfaces found. Using first.')
            iface = allIf[0]

        self.udpSocket = QtNetwork.QUdpSocket(self)
        self.udpSocket.readyRead.connect(self.readUdpSocket)
        self.udpSocket.bind(QtNetwork.QHostAddress.AnyIPv4, mcastPort,
                            QtNetwork.QUdpSocket.ShareAddress)
        self.udpSocket.joinMulticastGroup(
            QtNetwork.QHostAddress(mcastGroup),
            iface
        )

        self.logger.info(
            (f'Listening on {mcastGroup}:{mcastPort}'
             f' on interface {iface.name()}...'))

    def readUdpSocket(self):
        self.logger.debug('Datagram(s) received.')
        while self.udpSocket.hasPendingDatagrams():
            size = self.udpSocket.pendingDatagramSize()
            (data, host, port) = self.udpSocket.readDatagram(size)
            dataStr = data.decode('utf-8')
            self.logger.debug("Received from %s:%i: %s",
                              host.toString(), port, repr(dataStr))
            self.buf += dataStr
            self.logger.debug('buf: ' + repr(self.buf))

        idx = self.buf.find('\n')
        while idx > -1:
            line = self.buf[:idx].strip()
            self.buf = self.buf[idx + 1:]
            self.logger.debug('line: ' + repr(line))
            self.readLine.emit(line)
            idx = self.buf.find('\n')
        self.logger.debug('buf: ' + repr(self.buf))

# ----------------------------------------------------------------------------
