# ----------------------------------------------------------------------------
#
# Map helper functions
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import os
import math
import numpy as np

from PyQt5.QtGui import QPixmap, QPainter, QColor, QPainterPath
from PyQt5.QtCore import QPoint, QPointF, QRectF

from Projection import MercatorProjection

# ----------------------------------------------------------------------------

tileDim = 256


# ----------------------------------------------------------------------------

def deg2num(latDeg, lonDeg, zoom):
    lat_rad = math.radians(latDeg)
    n = 2.0 ** zoom
    xtile = int((lonDeg + 180.0) / 360.0 * n)
    ytile = int((1.0 - math.log(math.tan(lat_rad) +
                 (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
    return (xtile, ytile)


# ----------------------------------------------------------------------------

def num2deg(xtile, ytile, zoom):
    """
    http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    This returns the NW-corner of the square.
    Use the function with xtile+1 and/or ytile+1 to get the other corners.
    With xtile+0.5 & ytile+0.5 it will return the center of the tile.
    """
    n = 2.0 ** zoom
    lonDeg = xtile / n * 360.0 - 180.0
    lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
    latDeg = math.degrees(lat_rad)
    return (latDeg, lonDeg)


# ----------------------------------------------------------------------------

def meters_per_pixel(zoom, latDeg):
    r = 6372798.2
    C = 2 * math.pi * r
    return C / (2 ** (zoom + 8))  # equator


# ----------------------------------------------------------------------------

def meters_per_pixel2(zoom, latDeg):
    r = 6372798.2
    C = 2 * math.pi * r
    # return C / (2 ** (zoom + 8))  # equator
    return math.cos(latDeg * math.pi / 180.0) * C / (256 * 2**zoom)


# ----------------------------------------------------------------------------

class Proj:
    def __init__(self, proj, mpp, originX, originY, totHeight):
        self.proj = proj
        self.mpp = mpp
        self.originX = originX
        self.originY = originY
        self.totHeight = totHeight

    def __call__(self, lonDeg, latDeg):
        coord = self.proj(lonDeg, latDeg)
        px = np.array(coord) / self.mpp
        return QPointF(self.originX + px[0],
                       self.originY + self.totHeight - px[1])

    def inverse(self, x, y):
        # x = (coord[0] / mpp) + originX
        # y = originY + totHeight - (coord[1] / mpp)
        # -> coord[0] = (x - originX) * mpp
        # -> coord[1] = -(y - originY - totHeight) * mpp
        coord = (
            (x - self.originX) * self.mpp,
            -(y - self.originY - self.totHeight) * self.mpp)
        return self.proj.inverse(coord[0], coord[1])


# ----------------------------------------------------------------------------

def getCoordZoom(minLatDeg, maxLatDeg, minLonDeg, maxLonDeg, width, height,
                 maxZoom, defaultZoom, logger):

    # mean shall be center
    latDeg = (minLatDeg + maxLatDeg) / 2
    lonDeg = (minLonDeg + maxLonDeg) / 2
    latCos = math.cos(math.radians(latDeg))

    marginFactor = 1.3
    lonDiff = (maxLonDeg - minLonDeg) * marginFactor
    latDiff = (maxLatDeg - minLatDeg) * marginFactor / latCos

    minXTiles = math.ceil(width / tileDim) + 1
    minYTiles = math.ceil(height / tileDim) + 1

    # n = 2 ** z
    # M = al / 360 * 2 ** z
    # 2 ** z = M * 360 / al
    # z = log2(M * 360 / al)

    if latDiff and lonDiff:
        zoomX = math.log(minXTiles * 360.0 / lonDiff, 2.0)
        zoomY = math.log(minYTiles * 360.0 / latDiff, 2.0)
        zoom = int(round(min(zoomX, zoomY))) - 1
        logger.debug('Zoom: %f x %f => %u', zoomX, zoomY, zoom)

        limZoom = min(zoom, maxZoom)
        if limZoom != zoom:
            logger.debug('Limiting to %u', limZoom)
        zoom = limZoom
    else:
        zoom = defaultZoom

    return latDeg, lonDeg, zoom


# ----------------------------------------------------------------------------

def getPixmap(latDeg, lonDeg, zoom, width, height, config, logger):

    mpp = meters_per_pixel(zoom, latDeg)
    mpp2 = meters_per_pixel2(zoom, latDeg)

    x, y = deg2num(latDeg, lonDeg, zoom)
    tileLatDeg, tileLonDeg = num2deg(x, y, zoom)
    nextTileLatDeg, nextTileLonDeg = num2deg(x + 1, y + 1, zoom)

    proj = MercatorProjection(tileLonDeg, nextTileLatDeg,
                              nextTileLonDeg, tileLatDeg)
    coord = proj(lonDeg, latDeg)
    px = np.array(coord) / mpp

    center = QPoint(int(width / 2), int(height / 2))
    offset = QPoint(int(px[0]), int(tileDim - px[1]))
    centerTilePoint = center - offset

    xd = math.ceil(centerTilePoint.x() / tileDim)
    if xd < 0:
        xd = 0
    minX = x - xd

    yd = math.ceil(centerTilePoint.y() / tileDim)
    if yd < 0:
        yd = 0
    minY = y - yd

    originX = centerTilePoint.x() - xd * tileDim
    originY = centerTilePoint.y() - yd * tileDim

    numX = math.ceil((width - originX) / tileDim)
    numY = math.ceil((height - originY) / tileDim)
    totHeight = numY * tileDim

    lllat, lllon = num2deg(minX, minY + numY, zoom)
    urlat, urlon = num2deg(minX + numX, minY, zoom)
    totProj = MercatorProjection(lllon, lllat, urlon, urlat)
    proj = Proj(totProj, mpp, originX, originY, totHeight)

    pixmap = QPixmap(int(width), int(height))
    pixmap.fill()  # white
    painter = QPainter()
    painter.begin(pixmap)

    for i in range(0, numX):
        for j in range(0, numY):
            tile = getTile(minX + i, minY + j, zoom, config, logger)
            xp = originX + i * tileDim
            yp = originY + j * tileDim
            painter.drawPixmap(xp, yp, tile)

    centerLat = config.getfloat("area", "center_lat", fallback=None)
    centerLon = config.getfloat("area", "center_lon", fallback=None)
    if centerLat is not None and centerLon is not None:
        pos = proj(centerLon, centerLat)

        rect = QRectF(pos.x() - 4, pos.y() - 4, 8, 8)
        painter.drawEllipse(rect)

        index = 0
        prevRect = None
        while True:
            radius = config.getfloat("area", f"radius{index}", fallback=None)
            if radius is None:
                break
            d = radius / mpp2
            path = QPainterPath()
            rect = QRectF(pos.x() - d, pos.y() - d, 2 * d, 2 * d)
            path.addEllipse(rect)
            if prevRect:
                path.addEllipse(prevRect)
            painter.setBrush(QColor(config.get("area", f"color{index}",
                                               fallback="#400000c0")))
            painter.drawPath(path)
            prevRect = rect
            index += 1

    painter.end()

    return pixmap, proj


# ----------------------------------------------------------------------------

def getTile(x, y, zoom, config, logger):
    tilesDir = config.get("map", "tiles_dir", fallback="tiles")
    path = os.path.join(tilesDir, str(zoom), str(x), str(y) + '.png')
    # logger.info("Opening %s", path)
    tile = QPixmap()
    ok = tile.load(path)
    if not ok:
        logger.error("Opening image %s failed!", path)
    return tile

# ----------------------------------------------------------------------------
