# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Map Display Widget
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

from PyQt5.QtCore import Qt, QSize, QSizeF, QRectF, QTimer, QEvent
from PyQt5.QtCore import QPoint, QPointF
from PyQt5.QtWidgets import QFrame, QShortcut, QPushButton
from PyQt5.QtGui import QPainter, QPen, QKeySequence

import Map

import datetime
import math as m
import qtawesome as qta


# ----------------------------------------------------------------------------

class MapWidget(QFrame):

    def __init__(self, parent, config, vehicles, logger):
        super(MapWidget, self).__init__(parent)

        self.config = config
        self.vehicles = vehicles
        self.logger = logger
        self.pixmap = None
        self.proj = None

        self.updateTimer = QTimer()
        self.updateTimer.timeout.connect(self.update)
        self.updateTimer.start(1000)

        self.touchPanning = False
        self.touchStartPos = QPointF()
        self.touchEndPos = QPointF()
        self.mouseInside = False
        self.mousePos = QPointF()
        self.autoPan = True
        self.latDeg = config.getfloat("map", "default_lat", fallback=0.0)
        self.lonDeg = config.getfloat("map", "default_lon", fallback=0.0)
        self.maxZoom = config.getint("map", "max_zoom", fallback=17)
        self.defaultZoom = config.getint("map", "default_zoom", fallback=15)
        self.zoom = self.defaultZoom

        self.setAttribute(Qt.WA_AcceptTouchEvents, True)
        self.setMouseTracking(True)

        shortcut = QShortcut(QKeySequence("+"), self)
        shortcut.activated.connect(self.zoomIn)
        shortcut = QShortcut(QKeySequence("-"), self)
        shortcut.activated.connect(self.zoomOut)
        shortcut = QShortcut(QKeySequence("A"), self)
        shortcut.activated.connect(self.enableAutoPan)

        icon = qta.icon('fa.expand', color='white', color_active='orange')
        self.buttonExpand = QPushButton(icon, '', parent)
        self.buttonExpand.setIconSize(QSize(48, 48))
        self.buttonExpand.clicked.connect(self.enableAutoPan)
        self.buttonExpand.setEnabled(not self.autoPan)
        self.buttonExpand.raise_()

        icon = qta.icon('fa.search-plus', color='white',
                        color_active='orange')
        self.buttonZoomIn = QPushButton(icon, '', parent)
        self.buttonZoomIn.setIconSize(QSize(48, 48))
        self.buttonZoomIn.clicked.connect(self.zoomIn)
        self.buttonZoomIn.raise_()

        icon = qta.icon('fa.search-minus', color='white',
                        color_active='orange')
        self.buttonZoomOut = QPushButton(icon, '', parent)
        self.buttonZoomOut.setIconSize(QSize(48, 48))
        self.buttonZoomOut.clicked.connect(self.zoomOut)
        self.buttonZoomOut.raise_()

        self.invalidate()

    def invalidate(self):
        self.updateMap()

    def updateMap(self):
        if self.autoPan:
            now = datetime.datetime.now()
            (minLatDeg, maxLatDeg, minLonDeg, maxLonDeg) = \
                self.vehicles.getPositionRange(now)
            if minLatDeg is not None and maxLatDeg is not None and \
                    minLonDeg is not None and maxLonDeg is not None:
                self.latDeg, self.lonDeg, self.zoom = Map.getCoordZoom(
                    minLatDeg, maxLatDeg, minLonDeg, maxLonDeg,
                    self.contentsRect().width(), self.contentsRect().height(),
                    self.maxZoom, self.defaultZoom, self.logger)

        self.pixmap, self.proj = Map.getPixmap(self.latDeg, self.lonDeg,
                                               self.zoom,
                                               self.contentsRect().width(),
                                               self.contentsRect().height(),
                                               self.config, self.logger)
        self.update()

    def resizeEvent(self, event):
        self.updateMap()
        self.buttonExpand.setGeometry(
                self.contentsRect().right() - 80,
                self.contentsRect().bottom() - 80,
                60, 60)
        self.buttonZoomOut.setGeometry(
                self.contentsRect().right() - 80,
                self.contentsRect().bottom() - 160,
                60, 60)
        self.buttonZoomIn.setGeometry(
                self.contentsRect().right() - 80,
                self.contentsRect().bottom() - 240,
                60, 60)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing +
                              QPainter.SmoothPixmapTransform)
        now = datetime.datetime.now()

        if self.pixmap:
            painter.drawPixmap(0, 0, self.pixmap)

        if self.proj:
            markerSize = QSizeF(7, 7)
            markerOffset = QPointF(markerSize.width() / 2.0,
                                   markerSize.height() / 2.0)

            # get visible vehicles
            visible = []
            for v in self.vehicles.getPositionVehicles(now):
                latDeg, lonDeg = v.getAnimatedCoords()
                pos = self.proj(lonDeg, latDeg)
                cr = QRectF(self.contentsRect())
                if cr.contains(pos):
                    visible.append(v)

            # sort vehicles weighted with neighborhood density
            neighborMap = {}
            for v in visible:
                n = 0.0
                latDeg, lonDeg = v.getAnimatedCoords()
                vPos = self.proj(lonDeg, latDeg)
                for o in visible:
                    if v == o:
                        continue
                    latDeg, lonDeg = o.getAnimatedCoords()
                    oPos = self.proj(lonDeg, latDeg)
                    dX = oPos.x() - vPos.x()
                    dY = oPos.y() - vPos.y()
                    dist = m.sqrt(dX * dX + dY * dY)
                    if dist <= 0.0:
                        dist = 1.0
                    n += 1.0 / dist**2
                neighborMap[v] = n

            drawList = sorted(neighborMap, key=neighborMap.get, reverse=True)
            blockRects = []

            # draw circle markers
            for v in drawList:
                latDeg, lonDeg = v.getAnimatedCoords()
                pos = self.proj(lonDeg, latDeg)
                markerRect = QRectF(pos - markerOffset, markerSize)
                painter.setPen(QPen())
                painter.setBrush(Qt.black)
                painter.setOpacity(v.opacity(now))
                painter.drawEllipse(markerRect)
                painter.setOpacity(1.0)
                blockRects.append(markerRect)

            # draw labels
            offset = 20
            index = 0
            for v in drawList:
                index += 1

                labelRect = QRectF(QPointF(0.0, 0.0), v.labelSize())

                latDeg, lonDeg = v.getAnimatedCoords()
                pos = self.proj(lonDeg, latDeg)

                corner = pos + QPointF(offset, -offset)
                ankor = corner + v.bottomLeft()
                labelRect.moveBottomLeft(corner)
                block = False
                for b in blockRects:
                    if labelRect.intersects(b):
                        block = True
                        break

                if block:
                    corner = pos + QPointF(-offset, offset)
                    ankor = corner + v.topRight()
                    labelRect.moveTopRight(corner)
                    block = False
                    for b in blockRects:
                        if labelRect.intersects(b):
                            block = True
                            break

                if block:
                    corner = pos + QPointF(offset, offset)
                    ankor = corner + v.topLeft()
                    labelRect.moveTopLeft(corner)
                    block = False
                    for b in blockRects:
                        if labelRect.intersects(b):
                            block = True
                            break

                if block:
                    corner = pos + QPointF(-offset, -offset)
                    ankor = corner + v.bottomRight()
                    labelRect.moveBottomRight(corner)
                    block = False
                    for b in blockRects:
                        if labelRect.intersects(b):
                            block = True
                            break

                painter.setOpacity(v.opacity(now))
                painter.setPen(QPen(Qt.black, 2, Qt.SolidLine))
                painter.drawLine(pos, ankor)
                v.drawLabel(painter, labelRect, now, index)
                painter.setOpacity(1.0)
                blockRects.append(labelRect)

        if self.touchPanning:
            offset = 40
            painter.setPen(QPen(Qt.red))
            painter.drawLine(self.touchStartPos + QPoint(-offset, 0),
                             self.touchStartPos + QPoint(offset, 0))
            painter.drawLine(self.touchStartPos + QPoint(0, -offset),
                             self.touchStartPos + QPoint(0, offset))
            painter.setPen(QPen(Qt.blue))
            painter.drawLine(self.touchEndPos + QPoint(-offset, 0),
                             self.touchEndPos + QPoint(offset, 0))
            painter.drawLine(self.touchEndPos + QPoint(0, -offset),
                             self.touchEndPos + QPoint(0, offset))
            startLon, startLat = self.proj.inverse(self.touchStartPos.x(),
                                                   self.touchStartPos.y())
            endLon, endLat = self.proj.inverse(self.touchEndPos.x(),
                                               self.touchEndPos.y())
            painter.drawText(10, 10, f'{startLon:.4f} {startLat:.4f}')
            painter.drawText(10, 30, f'{endLon:.4f} {endLat:.4f}')

        if self.mouseInside:
            lonDeg, latDeg = self.proj.inverse(self.mousePos.x(),
                                               self.mousePos.y())
            painter.setPen(QPen(Qt.red))
            painter.drawText(self.contentsRect().left() + 10,
                             self.contentsRect().bottom() - 10,
                             f'{latDeg:.4f} {lonDeg:.4f}')

    def event(self, e):
        if e.type() in (QEvent.TouchBegin, QEvent.TouchUpdate,
                        QEvent.TouchEnd, QEvent.TouchCancel):
            return self.touchEvent(e)
        if e.type() in (QEvent.Enter, QEvent.MouseMove,
                        QEvent.Leave):
            return self.mouseEvent(e)
        return super(MapWidget, self).event(e)

    def touchEvent(self, e):
        count = len(e.touchPoints())
        if e.type() == QEvent.TouchBegin:
            if count == 1:
                self.touchEndPos = e.touchPoints()[0].pos()
                self.touchStartPos = self.touchEndPos
                self.startLatDeg = self.latDeg
                self.startLonDeg = self.lonDeg
                self.touchPanning = True
                self.update()
                return True
        elif e.type() == QEvent.TouchUpdate:
            if count == 1:
                self.touchEndPos = e.touchPoints()[0].pos()
                startLon, startLat = self.proj.inverse(self.touchStartPos.x(),
                                                       self.touchStartPos.y())
                endLon, endLat = self.proj.inverse(self.touchEndPos.x(),
                                                   self.touchEndPos.y())
                lonDiff = endLon - startLon
                latDiff = endLat - startLat
                self.latDeg = self.startLatDeg - latDiff
                self.lonDeg = self.startLonDeg - lonDiff
                self.setAutoPan(False)
                self.updateMap()
                return True
        elif e.type() == QEvent.TouchEnd:
            self.touchPanning = False
            self.update()
            return True
        elif e.type() == QEvent.TouchCancel:
            self.touchPanning = False
            self.latDeg = self.startLatDeg
            self.lonDeg = self.startLonDeg
            self.updateMap()
            return True
        return False

    def mouseEvent(self, e):
        if e.type() == QEvent.Enter:
            self.mousePos = e.pos()
            self.mouseInside = True
            self.update()
            return False
        elif e.type() == QEvent.MouseMove:
            self.mousePos = e.pos()
            self.update()
            return False
        elif e.type() == QEvent.Leave:
            self.mouseInside = False
            self.update()
            return False
        return False

    def wheelEvent(self, e):
        e.accept()
        if e.angleDelta().y() > 0:
            self.zoomIn()
        elif e.angleDelta().y() < 0:
            self.zoomOut()

    def zoomIn(self):
        if self.zoom >= self.maxZoom:
            return
        self.zoom += 1
        self.setAutoPan(False)
        self.updateMap()

    def zoomOut(self):
        if self.zoom <= 0:
            return
        self.zoom -= 1
        self.setAutoPan(False)
        self.updateMap()

    def enableAutoPan(self):
        self.setAutoPan(True)

    def setAutoPan(self, state):
        oldState = self.autoPan
        self.autoPan = state
        self.buttonExpand.setEnabled(not state)
        if self.autoPan and not oldState:
            self.updateMap()

# ----------------------------------------------------------------------------
