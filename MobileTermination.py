# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Mobile Termination List
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import csv

# ----------------------------------------------------------------------------


class MobileTermination:

    names = {}
    csv_path = 'fugs.csv'

    @classmethod
    def set_path(cls, path):
        cls.csv_path = path

    @classmethod
    def read_csv(cls):
        with open(cls.csv_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                cls.names[row['TSI']] = row['Opta']

    def __init__(self, identifier, name=None):

        if not name:
            if not self.names:
                self.read_csv()
            if identifier in self.names:
                name = self.names[identifier]

        self.identifier = identifier
        self.name = name

    def __repr__(self):
        return f'{self.identifier} {self.name}'

# ----------------------------------------------------------------------------
