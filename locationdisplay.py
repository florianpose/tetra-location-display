#!/usr/bin/python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
#
# Tetra Location Display Entry Function
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import sys
import configparser
import logging
import argparse
import traceback
import signal

import PyQt5.QtWidgets
from PyQt5.QtCore import Qt

import MainWidget
from SignalWakeupHandler import SignalWakeupHandler

# ----------------------------------------------------------------------------

logger = logging.getLogger('locationdisplay')


# ----------------------------------------------------------------------------

def signal_handler(signum, frame):
    logger.info("Received signal %i.", signum)
    PyQt5.QtWidgets.QApplication.quit()


# ----------------------------------------------------------------------------

def application_excepthook(exc_type, exc_value, exc_tb):
    tb = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    logger.error("Uncaught exception:\n%s", tb)
    PyQt5.QtWidgets.QApplication.quit()


# ----------------------------------------------------------------------------

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', '-v', action='count', default=0)
    parser.add_argument('config', nargs='*', default=[])
    parser.add_argument('--simulation', '--sim', '-s', action='store_true',
                        default=False)
    parser.add_argument('--display-all', '-a', action='store_true',
                        default=False)
    args = parser.parse_args()

    configPaths = [
        '/etc/locationdisplay.conf',
        '~/.locationdisplay.conf',
        'locationdisplay.conf'
        ]
    for c in args.config:
        configPaths.extend(c)
    config = configparser.ConfigParser()
    config.read(configPaths)

    if args.verbose > 0:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s %(message)s')

    streamHandler = logging.StreamHandler()
    streamHandler.setLevel(logging.DEBUG)
    streamHandler.setFormatter(formatter)
    logger.addHandler(streamHandler)

    logPath = config.get("display", "log_path",
                         fallback="/var/log/locationdisplay.log")
    fileHandler = logging.FileHandler(logPath)
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)

    logger.info('Starting up...')

    app = PyQt5.QtWidgets.QApplication(sys.argv)

    sys.excepthook = application_excepthook

    SignalWakeupHandler(app)
    catchable_sigs = (signal.SIGTERM, signal.SIGHUP)
    for sig in catchable_sigs:
        signal.signal(sig, signal_handler)

    app.setAttribute(Qt.AA_SynthesizeTouchForUnhandledMouseEvents, True)

    mainWidget = MainWidget.MainWidget(config, logger, args.simulation,
                                       args.display_all)
    mainWidget.showMaximized()

    logger.info('Starting application main loop.')

    try:
        ret = app.exec_()
    except Exception:
        ret = -1
        logger.error('Global exception caught:', exc_info=True)

    logger.info('Exiting.')
    mainWidget.saveState()
    sys.exit(ret)

# ----------------------------------------------------------------------------
