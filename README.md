vim: tw=78 spl=en spell

# Tetra Location Display

This is an vehicle location display implementation for fire departments and
other public safety/rescue organisations.

![Screenshot](images/title.png)

## Software

The software is written in Python3, using the PyQt libaries for Qt5.

## Features

- Tetra PEI dissector receiving data via multicast
- Maps using OpenStreetMap tiles or custom tiles with fire hydrants
