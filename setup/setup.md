# Install

Raspbian buster

```bash
unzip -p 2021-05-07-raspios-buster-armhf-lite.zip | sudo dd status=progress of=/dev/mmcblk0 bs=4M conv=fsync
```

# Raspi-config

- System Options / Hostname: ...
- Localisation Options / Locale: de_DE@UTF_8
- Localisation Options / Timezone: Europa/Berlin
- Localisation Options / Match Keyboard layout
- Interface Options / SSH / enable

# Kernel Configuration

Copy [this configuration file](config.txt) to /boot/config.txt

It sets the correct video mode and enables sound via HDMI.

# Network

```bash
vi /etc/dhcpcd.conf
```

```config
# Example static IP configuration:
interface eth0
static ip_address=...
static ip6_address=...
static routers=...
static domain_name_servers=...
```

# Users

```bash
passwd pi
passwd root
```

# Software

```bash
sudo apt-get update

sudo apt-get install \
    cec-utils \
    git \
    libsox-fmt-mp3 \
    pulseaudio-utils \
    python-libcec \
    python3-babel \
    python3-bitstring \
    python3-lxml \
    python3-numpy \
    python3-pip \
    python3-pyproj \
    python3-pyqt5 \
    python3-pyqt5.qtsvg \
    python3-qtawesome \
    python3-tzlocal \
    sox \
    vim \
    x11-xserver-utils \
    x11vnc \
    xinit

sudo apt-get upgrade

sudo pip3 install \
    pre-commit
```

# Location Software

```bash
git clone https://gitlab.com/florianpose/tetra-location-display.git

cd tetra-location-display
sudo cp locationdisplay-sample.conf /etc/locationdisplay.conf
sudo vi /etc/locationdisplay.conf

sudo cp locationdisplay.service /etc/systemd/system
sudo systemctl enable locationdisplay.service
sudo systemctl start locationdisplay.service
```
