# ----------------------------------------------------------------------------
#
# Vehicle Data
#
# Copyright (C) 2018-2022 Florian Pose
#
# This file is part of Tetra Location Display.
#
# Tetra Location Display is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# Tetra Location Display is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Tetra Location Display. If not, see <http://www.gnu.org/licenses/>.
#
# ----------------------------------------------------------------------------

import datetime

from PyQt5.QtCore import QObject, pyqtSignal, Qt
from PyQt5.QtCore import QPointF, QRectF, QSizeF
from PyQt5.QtCore import QVariant, QEasingCurve, QVariantAnimation
from PyQt5.QtGui import QPainterPath, QPen, QFontMetrics, QFont, QColor

# ----------------------------------------------------------------------------

statusColors = {
        1: '#17a2b8',
        2: '#28a745',
        3: '#ffc107',
        4: '#cb4b16',
        6: '#81000c',
        }


# ----------------------------------------------------------------------------

class Vehicle(QObject):

    moving = pyqtSignal()
    timeFormat = '%Y-%m-%d %H:%M:%S'

    def __init__(self, config, logger, mt):
        super(Vehicle, self).__init__()
        self.mt = mt
        self.logger = logger
        self.latDeg = None
        self.lonDeg = None
        self.positionTimestamp = None
        self.status = None
        self.statusTimeStamp = None

        self.anim = QVariantAnimation()
        self.anim.setDuration(3000)
        self.anim.setEasingCurve(QEasingCurve.InOutQuad)
        self.anim.valueChanged.connect(self.animValueChanged)

    def dumpState(self):
        return {
            'tsi': self.mt.identifier,
            'name': self.mt.name,
            'lat': self.latDeg,
            'lon': self.lonDeg,
            'time': self.positionTimestamp.strftime(self.timeFormat)
            if self.positionTimestamp else None,
            'status': self.status
        }

    def loadState(self, state):
        if state['lat'] is not None and state['lon'] is not None:
            self.latDeg = float(state['lat'])
            self.lonDeg = float(state['lon'])
            self.positionTimestamp = \
                datetime.datetime.strptime(state['time'], self.timeFormat)
        if state['status'] is not None:
            self.status = int(state['status'])

    def animValueChanged(self, value):
        self.moving.emit()

    def setPosition(self, latDeg, lonDeg, now=datetime.datetime.now()):
        if self.latDeg and self.lonDeg:
            self.anim.setStartValue(
                QVariant(QPointF(self.latDeg, self.lonDeg)))
            self.anim.setEndValue(QVariant(QPointF(latDeg, lonDeg)))
            self.anim.start()
        self.latDeg = latDeg
        self.lonDeg = lonDeg
        self.positionTimestamp = now

    def setStatus(self, status, now=datetime.datetime.now()):
        if status == 5:
            # Ignore request to talk
            return
        self.status = status
        self.statusTimestamp = now

    def positionValid(self, now):
        if self.latDeg is None or self.lonDeg is None \
                or self.positionTimestamp is None:
            return False

        dt = now - self.positionTimestamp
        ts = dt.total_seconds()
        return ts < 1800

    def __repr__(self):
        return (f'{self.mt.identifier} '
                f'{self.latDeg} {self.lonDeg} {self.status}')

    def getAnimatedCoords(self):
        pos = self.anim.currentValue()
        if pos is None:
            return self.latDeg, self.lonDeg
        else:
            return pos.x(), pos.y()

#   x--__-------__--x
#   |    --___--    |
#   |               |
#   |       x       |
#   |               |
#   x---------------x bottomRight
#   |__O_________O__|

    def labelSize(self):
        return QSizeF(100.0, 60.0)

    def topLeft(self):
        return QPointF(0.0, 0.0)

    def topRight(self):
        return QPointF(0.0, 0.0)

    def bottomLeft(self):
        return QPointF(0.0, -10.0)

    def bottomRight(self):
        return QPointF(0.0, -10.0)

    def opacity(self, now):
        dt = now - self.positionTimestamp
        ts = dt.total_seconds()
        if ts < 300:
            return 1.0
        if ts < 1800:
            return 0.4
        else:
            return 0.0

    def drawLabel(self, painter, rect, now, index):
        path = QPainterPath()
        path.moveTo(-50.0, -30.0)
        arcBound = QRectF(-50.0, -38.0, 100.0, 16.0)
        path.arcTo(arcBound, 180.0, 180.0)
        path.lineTo(50.0, 20.0)
        path.lineTo(-50.0, 20.0)
        path.lineTo(-50.0, -30.0)
        painter.translate(rect.center())
        painter.setPen(QPen(Qt.black, 2, Qt.SolidLine))
        if self.mt.name:
            painter.setBrush(Qt.red)
        else:
            painter.setBrush(Qt.gray)
        painter.drawPath(path)
        painter.setBrush(Qt.transparent)
        wheelBound = QRectF(-40.0, 20.0, 10.0, 10.0)
        painter.drawEllipse(wheelBound)
        wheelBound = QRectF(30.0, 20.0, 10.0, 10.0)
        painter.drawEllipse(wheelBound)

        dt = now - self.positionTimestamp
        ts = dt.total_seconds()
        mins = int(ts / 60)
        secs = int(ts % 60)
        delta = f'{mins}:{secs:02d}'
        if self.mt.name:
            title = self.mt.name
        else:
            title = self.mt.identifier
        textRect = QRectF(-47.0, -20.0, 94.0, 39.0)
        painter.setFont(QFont('Arial', 9, QFont.Bold))
        fm = QFontMetrics(painter.font())
        txt = fm.elidedText(title, Qt.ElideLeft, int(textRect.width()))
        painter.setPen(QPen(Qt.white))
        painter.drawText(textRect, Qt.AlignRight, txt)
        painter.setFont(QFont('Arial', 8))
        painter.drawText(textRect, Qt.AlignRight + Qt.AlignBottom, delta)
        if self.status in statusColors:
            color = QColor()
            color.setNamedColor(statusColors[self.status])
            painter.setPen(Qt.transparent)
            painter.setBrush(color)
            statusBound = QRectF(-47.0, -3.0, 20.0, 20.0)
            painter.drawRoundedRect(statusBound, 4, 4)
            statusText = f'{self.status}'
            painter.setPen(Qt.white)
            painter.setFont(QFont('Arial', 10, QFont.Bold))
            painter.drawText(statusBound, Qt.AlignCenter, statusText)

        painter.translate(-rect.center())

# ----------------------------------------------------------------------------
